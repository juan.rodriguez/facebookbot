'use strict'

const express = require('express');
const router = express.Router();
const request = require('request');

const FB_VERIFY_TOKEN = process.env.FB_VERIFY_TOKEN;
const PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN;

/* GET users listing. */
router.get('/', function(req, res, next) {
    if(req.query['hub.verify_token'] === FB_VERIFY_TOKEN) {
        res.send(req.query['hub.challenge']);
    }
    
    res.status(505).send(JSON.stringify({ "status" : "505", "message" : "Something broken!." }));
});

router.post('/', function(req, res) {
    var data = req.body;

    //Make sure this is a page suscription
    if(data.object === 'page') {

        //Iterate over each entry - there may be multiple if batched
        data.entry.forEach((entry) => {
            var pageID = entry.id;
            var timeOfEvent = entry.time;

            // Iterate over each messanging events
            entry.messaging.forEach((event) => {
                if(event.message) {
                    receivedMessage(event);
                } else {
                    console.log('webhook received unknown event: ', event);
                }
            });
        });

        // Assume all went well
        // you must send back a 200, within 20 seconds , to let us know
        // you've successfully recieved the callback. Otherwise, the request
        // will time out and we will keep trying to resend
        res.sendStatus(200);
    }
});

function receivedMessage(event) {
    var senderID = event.sender.id;
    var recipientID = event.recipient.id;
    var timeOfMessage = event.timestamp;
    var message = event.message;

    console.log('Recieved message for user %d and page %d at %d with message, ', senderID, recipientID, timeOfMessage);
    console.log(JSON.stringify(message));

    var messageID = message.id;
    var messageText = message.text;
    var messageAttachments = message.attachments;

    if(messageText) {
        // if we received a text message, check to see if it matches a keyword
        // and send back the example. Otherwise, just echo the text we received.
        switch(messageText) {
            case 'izum':
                sendGenericMessage(senderID);
                break;
            default:
                sendTextMessage(senderID, messageText);
        }
    } else if(messageAttachments) {
        sendTextMessage(senderID, 'Message with attachment received.');
    }
}

function sendGenericMessage(recipientID) {
    let messageData = {
        "attachment": {
            "type": "template",
            "payload": {
                "template_type": "generic",
                "elements": [{
                    "title": "First card",
                    "subtitle": "Element #1 of an hscroll",
                    "image_url": "http://inversioneszum.force.com/servlet/servlet.FileDownload?file=00P34000015DtS7EAK",
                    "buttons": [{
                        "type": "web_url",
                        "url": "http://inversioneszum.force.com/detallebien?idp=a0I3400000PVpbuEAD",
                        "title": "web url"
                    }, {
                        "type": "postback",
                        "title": "Postback",
                        "payload": "Payload for first element in a generic bubble",
                    }],
                }, {
                    "title": "Second card",
                    "subtitle": "Element #2 of an hscroll",
                    "image_url": "http://inversioneszum.force.com/servlet/servlet.FileDownload?file=00P34000015DtSMEA0",
                    "buttons": [{
                        "type": "web_url",
                        "url": "http://inversioneszum.force.com/detallebien?idp=a0I3400000PVpbuEAD",
                        "title": "web url"
                    }],
                }]
            }
        }
    }

    request({
        'url' : 'https://graph.facebook.com/v2.6/me/messages',
        'qs' : { 'access_token' : PAGE_ACCESS_TOKEN },
        'method' : 'POST',
        'json' : {
            'recipient' : { 'id' : recipientID },
            'message' : messageData,
        }
    }, function(error, response, body) {
        if (error) {
            console.log('Error sending messages: ', error)
        } else if (response.body.error) {
            console.log('Error: ', response.body.error)
        }
    })
}

function sendTextMessage(recipientID, messageText) {
    var messageData = {
        recipient: {
            'id' : recipientID
        },
        message: {
            'text' : messageText
        }
    }

    callSendAPI(messageData);
}

function callSendAPI(messageData) {
    request({
        'uri' : 'https://graph.facebook.com/v2.6/me/messages',
        'qs' : { 'access_token' : PAGE_ACCESS_TOKEN },
        'method' : 'POST',
        'json' : messageData
    }, function(error, response, body) {
        if(!error && response.statusCode == 200) {
            var recipientId = body.recipient_id;
            var messageId = body.message_id;

            console.log('Successfully sent generic message with id %s to recipient %s', messageId, recipientId);
        } else {
            console.error('Unable to send message');
            console.error(response);
            console.error(error);
        }
    });
}

module.exports = router; 
